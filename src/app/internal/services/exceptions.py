class NoPhoneNumberException(Exception):
    """Exception for user with no phone number."""

    def __init__(self, user_id: int) -> None:
        super().__init__(f"User with {user_id=} hasn't a phone number.")
