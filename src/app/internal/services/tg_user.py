from typing import Optional

from phonenumber_field.phonenumber import PhoneNumber

from app.internal.models.tg_user import TgUser

from .exceptions import NoPhoneNumberException


class TgUserService:
    def update_or_create(
        self, user_id: int, username: Optional[str] = None, phone_number: Optional[str] = None
    ) -> None:
        """Update or create TgUser."""

        defaults = {}

        if username is not None:
            defaults["username"] = username

        if phone_number is not None:
            PhoneNumber.from_string(phone_number).is_valid()
            defaults["phone_number"] = phone_number

        TgUser.objects.update_or_create(id=user_id, defaults=defaults)

    def get(self, user_id: int) -> Optional[TgUser]:
        """Get TgUser."""

        try:
            return TgUser.objects.get(id=user_id)

        except TgUser.DoesNotExist:
            return None

    def get_with_phone_number(self, user_id: int) -> Optional[TgUser]:
        """Get TgUser if he has a phone_number."""

        if (user := self.get(user_id)) is None:
            return None

        if user.phone_number is None:
            raise NoPhoneNumberException(user_id)

        return user
