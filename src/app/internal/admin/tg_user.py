from django.contrib import admin

from app.internal.models.tg_user import TgUser


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "phone_number")
