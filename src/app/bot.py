import signal
import sys
from dataclasses import dataclass, field
from typing import Any

from telegram.ext import CommandHandler, ConversationHandler, Updater

from app.internal.transport.bot import handlers
from config.settings import TG_BOT_TOKEN


@dataclass
class TgBot:
    """Class wrapper TgBot for updater."""

    _token: str
    _updater: Updater = field(init=False)

    def __post_init__(self) -> None:
        """Post bot's constructor."""

        self._updater = Updater(token=self._token)
        entry_points = [
            CommandHandler("start", handlers.start_handler),
            CommandHandler("me", handlers.me_handler),
            CommandHandler("set_phone", handlers.set_phone_handler),
            CommandHandler("help", handlers.help_handler),
        ]

        conversation_handler = ConversationHandler(
            entry_points=entry_points, allow_reentry=True, states={}, fallbacks=[]
        )
        self._updater.dispatcher.add_handler(conversation_handler)

    def run(self) -> None:
        """Run bot's polling."""

        self._updater.start_polling()


def run():
    """Function for run bot."""

    bot = TgBot(TG_BOT_TOKEN)
    bot.run()
