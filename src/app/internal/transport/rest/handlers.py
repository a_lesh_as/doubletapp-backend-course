from django.http.response import JsonResponse

from app.internal import constants as c
from app.internal.services.exceptions import NoPhoneNumberException
from app.internal.services.tg_user import TgUserService


def me_handler(request, id: int) -> JsonResponse:
    """Handler for endpoint tg_users/<int:id>/me"""

    tg_user_service = TgUserService()

    try:
        user = tg_user_service.get_with_phone_number(user_id=id)
        if user is None:
            return JsonResponse({"message": "Not found"}, status=404)

    except NoPhoneNumberException:
        return JsonResponse({"message": c.NO_ACCESS_MESSAGE}, status=403)

    return JsonResponse({"id": user.id, "username": user.username, "phone_number": str(user.phone_number)}, status=200)
