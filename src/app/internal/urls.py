from django.urls import path

from .transport.rest import handlers

urlpatterns = [
    path("tg_users/<int:id>/me", handlers.me_handler),
]
