from django.db import models
from phonenumber_field import modelfields


class TgUser(models.Model):
    id = models.BigIntegerField(primary_key=True, verbose_name="telegram_id")
    username = models.TextField(null=True)
    phone_number = modelfields.PhoneNumberField(null=True)

    def __str__(self) -> str:
        return f"telegram user_id: {self.id},\nusername: {self.username},\nphone number: {self.phone_number}"
