from phonenumbers.phonenumberutil import NumberParseException

from app.internal import constants as c
from app.internal.services.exceptions import NoPhoneNumberException
from app.internal.services.tg_user import TgUserService


def start_handler(update, _):
    """Handler for start command."""

    update.message.reply_text(c.BOT_START_MESSAGE)
    tg_user_service = TgUserService()

    user = update.effective_user

    tg_user_service.update_or_create(user_id=user.id, username=user.username)


def help_handler(update, _):
    """Handler for help command."""

    update.message.reply_text(c.BOT_HELP_MESSAGE)


def set_phone_handler(update, _):
    """Handler for set_phone command."""

    tg_user_service = TgUserService()
    user = update.effective_user

    try:
        phone_number = update.message.text.split()[1]

    except IndexError:
        update.message.reply_text(c.BOT_EMPTY_INPUT_MESSAGE)

    try:
        tg_user_service.update_or_create(user.id, user.username, phone_number)
        update.message.reply_text(c.BOT_SAVED_PHONE_MESSAGE)

    except NumberParseException:
        update.message.reply_text(c.BOT_WRONG_PHONE_MESSAGE)


def me_handler(update, _):
    """Handler for me command."""

    tg_user_service = TgUserService()

    try:
        user = tg_user_service.get_with_phone_number(user_id=update.effective_user.id)
        update.message.reply_text(f"FYI:\n{user}")

    except NoPhoneNumberException:
        update.message.reply_text(c.NO_ACCESS_MESSAGE)
